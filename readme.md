# Forkio App

Step project of `DAN.IT` school.

#### In this project we use:

- <b>Gulp</b> as a task manager.
- <b>NPM</b> for searching node-modules.
- <b>GIT</b> as a controle-version system.
- and <b>GitLab</b> for exchenge files

#### Commands for console:

- `npm install` - after downloading this repo, type this command in the console and it automatically installs all modules that necessary to run the task manager
- `npm start` - will start the task manager (for more details you can look into `package.json`)

#### Tasks & Colaborators:

- Задание студента №2 - [Илья Ромащенко](https://gitlab.com/romule)
- Задание студента №1 - [Никита Яковюк](https://gitlab.com/jakovyukjakov2012) & [Dmitriy Antoschenko](https://gitlab.com/dantonmail)

---

All tasks and intimations you can find [heare](https://gitlab.com/dan-it/dnepr-groups/fe2/-/tree/master/step-project-forkio)
